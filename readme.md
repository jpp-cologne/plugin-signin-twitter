
Twitter Signin for Datawrapper
==============================

To install this plugin, you need to set up a new app in Twitter and
obtain the consumer_key and consumer_secret for it.

Then you put both into ``config.yaml`` like this:

```
   # in 'plugins' section
   signin-twitter:
      consumer_key: xxxxxxxxx
      consumer_secret: xxxxxxxxxxxxxxxxx
```